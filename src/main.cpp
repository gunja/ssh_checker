#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>

#include <pthread.h>

#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>

#include <list>
//#include <array>

#include "ApplicationConfiguration.h"
#include "ThreadFunc.h"
#include "AssignmentManager.h"

#define DEF_LISTN   43012
#define DEF_CONF_PATH   "checker.conf"

uint16_t portLi;
int g_keep_terminal;
typedef int socket_t;

int printHelp()
{
    printf("calling conversion:\n");
    printf("appli [-c configfile] [-d] [-p portnum] [-l logfile] [-h]\n");
    printf("Options are the following:\n");
    printf("-c configfile\t\t path to file with configuration. ./checker.conf is default\n");
    printf("-d           \t\t keep on terminal ( don't go daemon\n");
    printf("-p portnum   \t\t open port portnum for incoming connections. default is 43012.");
        printf(" This option will override setting from configuration file\n");
    printf("-l logfile   \t\t path to file which will be used for logging. /tmp/checker.log is default\n");
    printf("-h           \t\t print this help and exit\n");
}


int main(int argc, char * argv[])
{
    // TODO implement 
    int ch;
    uint16_t portArgs=0;
    char  * logFilePath= NULL;
    char * paramsFileName = NULL;
    AppConf config;
    pthread_mutex_t commonMutex= PTHREAD_MUTEX_INITIALIZER;;
    pthread_cond_t  commonCond= PTHREAD_COND_INITIALIZER;
    pthread_mutex_t reportMutex= PTHREAD_MUTEX_INITIALIZER;;
    config.sPort( DEF_LISTN);
    g_keep_terminal = 0;
    while( (ch = getopt( argc, argv, "c:dp:l:h" ))!= -1)
    {
        switch( ch) {
          case 'c':
            paramsFileName = strdup( optarg);
            break;
          case 'd':
            g_keep_terminal = 1;
            break;
          case 'p':
            portArgs = atoi( optarg);
            break;
          case 'l':
            logFilePath = strdup( optarg);
            break;
          case 'h': default:
            printHelp();
            exit(0);
            break;  // избыточен
        }
    }
    if( logFilePath == NULL || ! config.loadConfiguration(logFilePath) )
        config.loadConfiguration(DEF_CONF_PATH);
    if( portArgs > 2000) config.sPort(portArgs);
    // OK. configuration loaded. Let's Rock-n-Roll
    config.KeepWork = true;
    std::list<TaskPtr> globalTasks;
    Manager managger;
    checkThreadParamPtr manager_params =
        new checkThreadParam( &config, &commonMutex, &commonCond, &reportMutex,
                &globalTasks);
    manager_params->setManager( &managger);

    // start required number of threads
        // start manager: create mutex on queue variable for reporting
        // etc
        int return_code =0;
        int result;
        pthread_attr_t threadAttr;
        pthread_attr_init(&threadAttr);
        pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_DETACHED); 
        pthread_t thread, thread_mana;
        result = pthread_create(&thread_mana, NULL, ManagerThreadFunc,
                static_cast<void*>(manager_params) );
        if( result != 0)
        {
            fprintf(stderr,
			"Creating thread false. Error: %d\nExiting\n", result);
            exit(1);
        }
        std::list<checkThreadParamPtr> params_array;
        for( int i=0; i < config.Parallel(); ++i)
        {
            checkThreadParamPtr p = new checkThreadParam(&config, &commonMutex,
                    &commonCond, &reportMutex, &globalTasks);
            params_array.push_back( p);
            // TODO known problem: out of memory exception
            result = pthread_create(&(p->thre), NULL, checkThreadFunction,
                    static_cast<void *>(p));
            // TODO known problem: result < 0 not checked
        }
    // open listening port
        socket_t listen_socket = socket( AF_INET, SOCK_STREAM, 0);
        if( listen_socket < 0)
        {
            fprintf(stderr, "Error on creating listening socket\n");
            config.KeepWork = false;
            return_code = -1;
            goto exiting_zone;
            // TODO signal all checker threads to exit
        }
        int cfd;
        struct sockaddr_in my_addr, peer_addr;
        socklen_t peer_addr_size;
        memset(&my_addr, 0, sizeof(struct sockaddr_in));
/* Clear structure */
        my_addr.sin_family = AF_INET;
        my_addr.sin_port = htons(config.port());
        my_addr.sin_addr.s_addr = INADDR_ANY;

        if (bind(listen_socket, reinterpret_cast<struct sockaddr *>(&my_addr),
                    sizeof(struct sockaddr_in)) == -1)
        {
            fprintf(stderr, "Error on binding to port %d\nExiting\n",
                        config.port());
            config.KeepWork = false;
            return_code = -2;
            goto exiting_zone;
        }
        if (listen(listen_socket, 5) == -1)
        {
            fprintf(stderr,
			"Error on calling listen for binded socket\n Exit\n");
            // TODO clear all memory
            return_code = -3;
            goto exiting_zone;
        }

        /* Now we can accept incoming connections one 
		at a time using accept(2) */
        while( config.KeepWork )
        {
            peer_addr_size = sizeof(struct sockaddr_in);
            int * cc = new int;
            *cc = cfd = accept(listen_socket, (struct sockaddr *)&peer_addr,
                    &peer_addr_size);
            if (cfd == -1)
            {
                // TODO handle this error
                continue;
            }
            // TODO this part is absolutely incomplete
            connectionThreadParamPtr p = new connectionThreadParam(&config,
			&commonMutex, &commonCond, &reportMutex);
            p->cc = cc;
            p->setManager( &managger);
            // TODO known problem: out of memory exception
            result = pthread_create(&thread, &threadAttr, connectionThreadFunc,
                    static_cast<void *>(p));
            
        }
exiting_zone:
        pthread_join( thread_mana, NULL);
        const int Num = 2* params_array.size();
        for(int i=0; i < Num; ++i)
        {
            pthread_cond_signal( &commonCond );
        }
        while( params_array.size() >0)
        {
            checkThreadParamPtr p = params_array.front();
            params_array.pop_front();
            pthread_join(p->thre, NULL);
            delete p;
        }
        delete manager_params;
        exit(return_code);
}

