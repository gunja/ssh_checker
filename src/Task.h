#ifndef TASK_DESC_H
#define TASK_DESC_H
#include <string>
#include <sys/time.h>
#include <utility>
#include <list>
#include <libssh2.h>

class AppConf;
class Assignment;

class RequestPart
{
public:
	unsigned short int port;
	std::string HostName;
	std::string pathPart;
};

class Task
{
    bool cmpld;
    bool cr;
    bool sshOk;
    struct timeval lastAttempt;
        std::string hostname;
        std::string username;
        std::string password;
        Assignment &container;
        void checkCompleted();
	RequestPart splitPath(std::string);
	LIBSSH2_SESSION * establishConnection(const int);
        void placeChecked(std::string ad, float v);
    std::list<std::pair<std::string, float> > cheched_nodes;
    public:
        // members
        // methods
        Task(Assignment & ass): cmpld(false),cr(false),container(ass){};
        Task( std::string, Assignment &);
        bool isCompleted() const { return cmpld;}
        bool isCorrect() const { return cr;}
        bool canBeScheduled(const struct timeval &t_cmp, const int) const;
        bool execute( AppConf *);
        //void setCNT( Assignment * a){ container = a;};
};
typedef Task * TaskPtr;

#endif
