#include "Task.h"
#include "ApplicationConfiguration.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <vector>

Task::Task( std::string ln, Assignment &co):
	cmpld(false), cr(false), container(co)
{
	// TODO
//198.58.126.7;admin;qwerty
//208.73.200.231;test;testtest
    // делаем дела. если успешно
	std::size_t ps = ln.find(";");
	if( ps == std::string::npos )
		return;
	hostname = ln.substr(0, ps);
	++ps;
	std::size_t ps2 = ln.find(";", ps );
	if( ps2 == std::string::npos ) return;
	username = ln.substr(ps, ps2 - ps);
	ps = ps2+1;
	ps2 = ln.find(";", ps );
	if( ps2 == std::string::npos ) password = ln.substr(ps);
	else password = ln.substr(ps, ps2 - ps);
// KNOWN TODO add port fetch and previous speed fetch
	if( hostname != "" && username != "" && password != "") cr = true;
}

bool Task::canBeScheduled(const struct timeval &t_cmp,const int delta_ms) const
{
    return 1000 * ( t_cmp.tv_sec - lastAttempt.tv_sec 
                + 1e-6*(t_cmp.tv_usec - lastAttempt.tv_usec )) > delta_ms;
}

LIBSSH2_SESSION * Task::establishConnection(const int sfd)
{
    LIBSSH2_SESSION *session = NULL;
    int rc;
    sshOk = false;
    session = libssh2_session_init();
    if(!session)
    {
        fprintf(stderr, "Could not initialize SSH session!\n");
        sshOk = false;
        return NULL;
    }
    rc = libssh2_session_handshake(session, sfd);
    if(rc)
    {
        fprintf(stderr, "Error when starting up SSH session: %d\n", rc);
        sshOk = false;
        return NULL;
    }
    sshOk = true;
    //fingerprint = libssh2_hostkey_hash(session, LIBSSH2_HOSTKEY_HASH_SHA1);
    char *userauthlist = libssh2_userauth_list(session, username.c_str(),
		username.length());
    int ln;
    if( strstr(userauthlist, "password") == NULL )
    {
        fprintf(stderr, "auth with password is not supported\n");
        // att++
        goto shutdown;
    }
    if( libssh2_userauth_password(session, username.c_str(), password.c_str()))
    {
        fprintf(stderr, "Authentication by password failed.\n");
        goto shutdown;
    }

shutdown:
    libssh2_session_disconnect(session, "Client disconnecting normally");
    libssh2_session_free(session);
    return NULL;
}

RequestPart Task::splitPath(std::string req)
{
	RequestPart rv;
	std::size_t positionHeader;
	std::size_t positionPathPart;
	std::size_t len = std::string::npos;
	rv.port = 80;
	rv.pathPart = "/";
	if( req.compare(0,8, "https://")) rv.port = 433;
	positionHeader = req.find("://");
	if( positionHeader != std::string::npos)
		positionHeader +=3;
	else positionHeader = 0;
	positionPathPart = req.find("/", positionHeader );
	if( positionPathPart != std::string::npos ) {
		rv.pathPart = req.substr(positionPathPart );
		len = positionPathPart - positionPathPart;
	}
	rv.HostName = req.substr( positionHeader, len);
	return rv;
}

bool Task::execute( AppConf * ac)
{
    // TODO
    // make ssh connection with host
    int rc = libssh2_init (0);
    int sfd=-1, l_sock=-1, f_sock = -1, ln;
    struct sockaddr_in sin;
    struct addrinfo *result, *rp;
    LIBSSH2_SESSION *session;
    LIBSSH2_CHANNEL *channel = NULL;
    gettimeofday(&lastAttempt, NULL);
    if(rc != 0)
    {
        fprintf (stderr, "libssh2 initialization failed (%d)\n", rc);
        return false;
    }
    int sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    memset( &sin, 0, sizeof(  struct sockaddr_in));
    sin.sin_family = AF_INET;
    rc = getaddrinfo( hostname.c_str(), "22", NULL, &result);
    if( rc != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rc));
        return false;
    }
    for(rp = result; rp != NULL; rp = rp->ai_next)
    {
        // socket
        sfd = socket( rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if(sfd == -1) continue;
        if( connect( sfd, rp->ai_addr, rp->ai_addrlen) == 0)
            break;
        close( sfd);
    }
    if( rp == NULL)
    {
        fprintf(stderr, "Could not connect\n");
        return false;
    }
    freeaddrinfo(result);
    session = establishConnection(sfd);
    if( ! sshOk ) return sshOk;
    if( session == NULL) goto shutdown;
// gunja. check comes here
    ln = ac->cnCount();
    for(int i2 = 0; i2 < ln; ++i2 )
    {
        struct timeval tv_s, tv_e;
        char buff[2000];
        std::string adds = ac->CheckNode(i2);
        if( adds == "") continue;
        fprintf(stderr, "Making connection to remote %s:%d\n",
		adds.c_str(), 80);
        RequestPart rp = splitPath(adds);
        channel = libssh2_channel_direct_tcpip(session,
		rp.HostName.c_str(), rp.port );
        if (!channel) {
            fprintf(stderr, "Could not open the direct-tcpip channel!\n"
                    "(Note that this can be a problem at the server!"
                    " Please review the server logs.)\n");
            placeChecked( adds, -1.);
            continue;
        }
        // OK at this point we already have channel to adds
        std::string request = "GET " + rp.pathPart + " HTTP/1.1" +
		"\r\nHost: " + rp.HostName + "\r\n\r\n";

        int len = request.length();
        int wr = 0;
        while(wr < len)
        {
            int i = libssh2_channel_write(channel, request.c_str() + wr,
				len - wr );
                if (LIBSSH2_ERROR_EAGAIN == i)
                {
                    continue;
                }
                if (i < 0)
                {
                    fprintf(stderr, "libssh2_channel_write: %d\n", i);
                    // TODO пересмотреть вот этот выход. не удаётся записать - перейти к следующему месту
                    goto shutdown;
                }
                wr += i;
        }
        // OK now, request written. let's start time counter
        int j, total=0, mx=-1;
	int cont_len =-1;
        gettimeofday( &tv_s, NULL);
        do {
		j = libssh2_channel_read( channel, buff, 2000);
		// check for Content-Length: XXX and count bytes
		if( j >0) { total += j; buff[j]= '\0'; }
		if( cont_len < 0 ) {
			char *pptr = strstr( buff, "Content-Length:"  );
// TODO WARN известная проблема, если в Content-Length не полная строка передана.
// пока предполагаем, что atoi всегда сможет корректно отработать
			if( pptr != NULL ) cont_len = atoi( pptr + 16 );
		}
		if( mx < 0 && cont_len >= 0  ) {
			char * pptr = strstr( buff, "\r\n\r\n");
			if ( pptr != NULL ) mx = cont_len + pptr - buff + 4;
		}
        } while ( j > 0 && ((mx < 0 ) || total < mx  ) );
        // stop timer. calculate download speed 
        gettimeofday( &tv_e, NULL);
        // TODO записать результат вот этого измерения. перейти к другому измерению
        if( total > 0)
        {
            float speed = total / (tv_e.tv_sec - tv_s.tv_sec 
				+ 1e-6 * (tv_e.tv_usec - tv_s.tv_usec ));
            placeChecked( adds, speed);
        }
        if( channel != NULL) libssh2_channel_free(channel);
        channel = NULL;
    }

    // try to fetch each address consequently
    // save values

    // and return
    // if successful
    // TODO check if all hosts-to-check are completed and switch cmpld
    this->checkCompleted();
    if( cmpld ) { container.incDone(); }
shutdown:
    if( f_sock > -1)
        close( f_sock);
    if( l_sock > -1)
        close( l_sock);
    if( channel != NULL) libssh2_channel_free(channel);
    libssh2_session_disconnect(session, "Client disconnecting normally");
    libssh2_session_free(session);
    close(sfd);
    libssh2_exit();
    return false;
}

void Task::checkCompleted()
{
    // TODO
    for( std::list<std::pair<std::string, float> >::iterator
            i = cheched_nodes.begin(); i != cheched_nodes.end(); ++i)
    {
        if( (*i).second  < 0.f )
        {
            return;
        }
    }
    if( cheched_nodes.size() > 0)
       cmpld = true;
    return;
}

void Task::placeChecked(std::string ad, float v)
{
    for( std::list<std::pair<std::string, float> >::iterator
            i = cheched_nodes.begin(); i != cheched_nodes.end(); ++i)
    {
        if( (*i).first == ad )
        {
            (*i).second = v;
            return;
        }
    }
    cheched_nodes.push_back( std::pair<std::string, float>(ad, v));
    return;
}

